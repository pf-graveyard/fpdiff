/*
 * fpdiff — small utility to find duplicates in music collection
 * © 2014 Oleksandr "post-factum" Natalenko <oleksandr@natalenko.name>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * Derived from examples of:
 * Chromaprint -- Audio fingerprinting toolkit
 * Copyright (C) 2010-2011  Lukas Lalinsky <lalinsky@gmail.com>
 */

#include <chromaprint.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/opt.h>
#include <libswresample/swresample.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

int decode_audio_file(ChromaprintContext *chromaprint_context,
		const char *file_name,
		int max_length,
		int *duration)
{
	int
		ok = 0,
		remaining,
		length,
		consumed,
		codec_context_opened = 0,
		got_frame,
		stream_index;
	AVFormatContext *format_context = NULL;
	AVCodecContext *codec_context = NULL;
	AVCodec *codec = NULL;
	AVStream *stream = NULL;
	AVFrame *frame = NULL;
	SwrContext *convert_context = NULL;
	int max_dst_nb_samples = 0, dst_linsize = 0;
	uint8_t *dst_data[1] =
	{
		NULL
	};
	uint8_t **data;
	AVPacket packet;

	if (!strcmp(file_name, "-"))
		file_name = "pipe:0";

	if (avformat_open_input(&format_context, file_name, NULL, NULL) != 0)
	{
		fprintf(stderr, "ERROR: couldn't open the file\n");
		goto done;
	}

	if (avformat_find_stream_info(format_context, NULL) < 0)
	{
		fprintf(stderr, "ERROR: couldn't find stream information in the file\n");
		goto done;
	}

	stream_index = av_find_best_stream(format_context, AVMEDIA_TYPE_AUDIO, -1, -1, &codec, 0);
	if (stream_index < 0)
	{
		fprintf(stderr, "ERROR: couldn't find any audio stream in the file\n");
		goto done;
	}

	stream = format_context->streams[stream_index];

	codec_context = stream->codec;
	codec_context->request_sample_fmt = AV_SAMPLE_FMT_S16;

	if (avcodec_open2(codec_context, codec, NULL) < 0)
	{
		fprintf(stderr, "ERROR: couldn't open the codec\n");
		goto done;
	}
	codec_context_opened = 1;

	if (codec_context->channels <= 0)
	{
		fprintf(stderr, "ERROR: no channels found in the audio stream\n");
		goto done;
	}

	if (codec_context->sample_fmt != AV_SAMPLE_FMT_S16)
	{
		int64_t channel_layout = codec_context->channel_layout;
		if (!channel_layout)
			channel_layout = av_get_default_channel_layout(codec_context->channels);
		convert_context = swr_alloc_set_opts(NULL,
			channel_layout, AV_SAMPLE_FMT_S16, codec_context->sample_rate,
			channel_layout, codec_context->sample_fmt, codec_context->sample_rate,
			0, NULL);
		if (!convert_context)
		{
			fprintf(stderr, "ERROR: couldn't allocate audio converter\n");
			goto done;
		}
		if (swr_init(convert_context) < 0)
		{
			fprintf(stderr, "ERROR: couldn't initialize the audio converter\n");
			goto done;
		}
	}

	if (stream->duration != AV_NOPTS_VALUE)
		*duration = stream->time_base.num * stream->duration / stream->time_base.den;
	else if (format_context->duration != AV_NOPTS_VALUE)
		*duration = format_context->duration / AV_TIME_BASE;
	else
	{
		fprintf(stderr, "ERROR: couldn't detect the audio duration\n");
		goto done;
	}

	remaining = max_length * codec_context->channels * codec_context->sample_rate;
	chromaprint_start(chromaprint_context, codec_context->sample_rate, codec_context->channels);

	frame = av_frame_alloc();

	while (1)
	{
		if (av_read_frame(format_context, &packet) < 0)
			break;

		if (packet.stream_index == stream_index)
		{
			av_frame_unref(frame);

			got_frame = 0;
			consumed = avcodec_decode_audio4(codec_context, frame, &got_frame, &packet);
			if (consumed < 0)
			{
				fprintf(stderr, "WARNING: error decoding audio\n");
				continue;
			}

			if (got_frame)
			{
				data = frame->data;
				if (convert_context) {
					if (frame->nb_samples > max_dst_nb_samples)
					{
						av_freep(&dst_data[0]);
						if (av_samples_alloc(dst_data,
									&dst_linsize,
									codec_context->channels,
									frame->nb_samples,
									AV_SAMPLE_FMT_S16,
									1) < 0)
						{
							fprintf(stderr, "ERROR: couldn't allocate audio converter buffer\n");
							goto done;
						}
						max_dst_nb_samples = frame->nb_samples;
					}
					if (swr_convert(convert_context,
								dst_data,
								frame->nb_samples,
								(const uint8_t**)frame->data,
								frame->nb_samples) < 0)
					{
						fprintf(stderr, "ERROR: couldn't convert the audio\n");
						goto done;
					}
					data = dst_data;
				}
				length = MIN(remaining, frame->nb_samples * codec_context->channels);
				if (!chromaprint_feed(chromaprint_context, data[0], length))
					goto done;

				if (max_length)
				{
					remaining -= length;
					if (remaining <= 0)
						goto finish;
				}
			}
		}
		av_free_packet(&packet);
	}

finish:
	if (!chromaprint_finish(chromaprint_context))
	{
		fprintf(stderr, "ERROR: fingerprint calculation failed\n");
		goto done;
	}

	ok = 1;

done:
	if (frame)
		av_frame_free(&frame);
	if (dst_data[0])
		av_freep(&dst_data[0]);
	if (convert_context)
		swr_free(&convert_context);
	if (codec_context_opened)
		avcodec_close(codec_context);
	if (format_context)
		avformat_close_input(&format_context);
	return ok;
}

int main(int argc, char **argv)
{
	int
		max_length = 600,
		num_file_names = 0,
		num_failed = 0;
	int* duration;
	int32_t **raw_fingerprint;
	int* raw_fingerprint_size;
	char* file_name;
	char** file_names;
	ChromaprintContext** chromaprint_context;

	file_names = malloc(argc * sizeof(char*));
	for (int i = 1; i < argc; i++)
		file_names[num_file_names++] = argv[i];

	if (!num_file_names)
	{
		printf("usage: %s [OPTIONS] FILE...\n\n", argv[0]);
		return 2;
	}

	av_register_all();
	av_log_set_level(AV_LOG_ERROR);

	chromaprint_context = malloc(sizeof(ChromaprintContext*) * num_file_names);
	for (int i = 0; i < num_file_names; i++)
		chromaprint_context[i] = chromaprint_new(CHROMAPRINT_ALGORITHM_DEFAULT);

	raw_fingerprint = malloc(sizeof(uint32_t*) * num_file_names);
	raw_fingerprint_size = malloc(sizeof(uint32_t*) * num_file_names);
	duration = malloc(sizeof(int) * num_file_names);

#if defined(_OPENMP)
#pragma omp parallel for
#endif
	for (int i = 0; i < num_file_names; i++)
	{
		file_name = file_names[i];
		if (!decode_audio_file(chromaprint_context[i], file_name, max_length, &duration[i]))
		{
			fprintf(stderr, "ERROR: unable to calculate fingerprint for file %s, skipping\n", file_name);
			num_failed++;
			continue;
		}
		if (!chromaprint_get_raw_fingerprint(chromaprint_context[i], (void **)&raw_fingerprint[i], &raw_fingerprint_size[i]))
		{
			fprintf(stderr, "ERROR: unable to calculate fingerprint for file %s, skipping\n", file_name);
			num_failed++;
			continue;
		}
	}

	for (int i = 0; i < num_file_names; i++)
		for (int j = i + 1; j < num_file_names; j++)
		{
			int differences = 0;
			int min_size = MIN(raw_fingerprint_size[i], raw_fingerprint_size[j]);
			int total_bits = min_size * sizeof(uint32_t) * CHAR_BIT;
			for (int k = 0; k < min_size; k++)
				differences += __builtin_popcount(raw_fingerprint[i][k] ^ raw_fingerprint[j][k]);
			double similarity = 1.0 - (double)differences / (double)total_bits;
			if (similarity > 0.55)
				printf("|%s|\t|%s|\t|%lf|\n", file_names[i], file_names[j], similarity);
		}

	free(duration);
	for (int i = 0; i < num_file_names; i++)
	{
		chromaprint_dealloc(raw_fingerprint[i]);
		chromaprint_free(chromaprint_context[i]);
	}
	free(raw_fingerprint_size);
	free(raw_fingerprint);
	free(chromaprint_context);
	free(file_names);

	return num_failed ? 1 : 0;
}

